module gitlab.com/renit.services/promises

go 1.12

require (
	github.com/campoy/embedmd v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
)
